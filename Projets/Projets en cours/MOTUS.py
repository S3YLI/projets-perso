from colorama import Fore
from random import randint
from math import remainder


def choix_mot():
    """ Prend en argument un fichier txt de mots et en renvoie un aléatoirement sous forme de str """
    L=[]
    with open("liste_mots.txt" , "r") as fr:
        D = fr.read().split("\n")
        for mot in D:
            mot.strip()
            L.append(mot)
        nb = randint(0,len(L)-1)
        return L[nb]
        
        


class Motus():
    
    def __init__(self, secret:str):
        self.secret = secret.upper().strip()
        self.essais = []
        self.essais_max = 6
        self.taille_mot = 6
        self.lettre_alea = "*"
        
    
    @property
    def est_resolu(self):
        """ Renvoie True si le Motus est résolu, False autrement """
        return len(self.essais) > 0 and self.essais[-1] == self.secret
    
    @property 
    def essais_restants(self):
        """ Renvoie le nombre d'essais restants """
        pass
    
    def ajout_essai(self, rep):
        """ Ajoute la proposition du joueur donnée en argument sous forme de str à la liste d'essais """
        pass
    @property 
    def peut_jouer(self):
        """ Retourne True si le joueur peut jouer, False sinon """
        pass
    
    def essai(self, mot):
        """ Fonction qui prend en argument le mot proposé sous forme de str et retourne une liste composée des lettres du mot """
        pass
        
        
        
class Lettre():
    
    """ Classe qui gère chaque lettre du mot proposé """
    
    def __init__(self, lettre :str):
        self.lettre :str = lettre
        self.est_dans_mot :bool = False
        self.est_dans_bonne_place: bool = False
        
    def __repr__(self):
        """ Fonction qui retourne un str avec les informations suivantes: la lettre est, ou non à la bonne place dans le mot
            et si elle est dans le mot """
        return f"[{self.lettre} est_dans_mot: {self.est_dans_mot} est_dans_bonne_place: {self.est_dans_bonne_place}]"

    
        
        
    
        
    
        
        

def Jeu():
    
    """ Fonction qui gère le fonctionnement du jeu, c'est en l'appelant que le jeu se lance """
    
    prenom = input('Quel est votre prénom ? : ')
    print('Bonjour '+str(prenom)+', jouons au Motus !')
    
    motus = Motus(str(choix_mot()))
    
    jeu = motus.peut_jouer
    while jeu :
        
        rep = input('Proposez un mot :')
        rep = rep.upper()
        
        if len(rep) != motus.taille_mot:
            print(Fore.RED + f"Le mot doit contenir {motus.taille_mot} lettres !" + Fore.RESET)
            continue
        
        motus.ajout_essai(rep)
        resultat = motus.essai(rep)
        affichage(motus)
        
        if rep == motus.secret :
            print('Vous avez trouvé le mot !')
            jeu = False
        elif rep != motus.secret and len(motus.essais) < motus.essais_max :
            print("Ce n'est pas la bonne réponse..., il vous reste "+str(motus.essais_restants)+" essais.")
        else :
            print("Vous avez épuisé tous vos essais, le mot était : "+str(motus.secret))
        
        jeu = motus.peut_jouer
            
def affichage(motus):
    
    """ Fonction qui permet de créer une interface graphique dans la console en prenant comme argument un objet de type Motus """
    lignes = []
    for mot in motus.essais :
        res = motus.essai(mot)
        resultat_converti_str = convert_res_couleur(res)
        lignes.append(resultat_converti_str)
        
    for _ in range(motus.essais_restants):
        lignes.append(" ".join(["_"] * motus.taille_mot))
        
    bordure(lignes)   
        
def convert_res_couleur(res):
    """ Fonction qui prend en argument une liste de lettres. 
        Elle permet de colorier chaque lettre en fonction de si elle est ou non dans le mot et si elle est à la bonne place
        dans le mot """
    res_couleur = []
    for lettre in res:
        if lettre.est_dans_bonne_place:
            couleur = Fore.GREEN
        elif lettre.est_dans_mot:
            couleur = Fore.YELLOW
        else :
            couleur = Fore.RED
        lettres_colorees = couleur + lettre.lettre + Fore.RESET
        res_couleur.append(lettres_colorees)
    return " ".join(res_couleur)

def bordure(lignes, taille: int=11, espace: int = 1):
    """ Fonction qui permet de dessiner les bordures extérieures de l'interface dans la console
        Prend en argument des entiers et une liste de str"""
    taille_totale = taille + espace * 2
    bordure_haut = "┌" + "─" * taille_totale + "┐"
    bordure_bas  = "└" + "─" * taille_totale + "┘"
    esp = " " * espace
    print(bordure_haut)
    
    for ligne in lignes:
        print("│" + esp + ligne + esp + "│")
        
    print(bordure_bas)
    
Jeu()


