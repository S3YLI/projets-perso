import pygame as pg
import time
from random import *

pg.init()

bleu = (0,0,255)
rouge = (255,0,0)
blanc = (255,255,255)
noir = (0,0,0)
vert = (50,205,50)

screen = pg.display.set_mode((400,400))
screen.fill(vert)
pg.display.update()
pg.display.set_caption('Snake')

clock = pg.time.Clock()

snake_bloc = 10
snake_vitesse = 30

font_style = pg.font.SysFont(None, 50)

def message(msg,color):
    msg = font_style.render(msg, True, color)
    screen.blit(msg, [0,200])
    
def Jeu():
    
    game_over = False
    jeu_ferme = False
    
    x1 = 200
    y1 = 200
    
    x1_change = 0
    y1_change = 0
    
    pommex = round(randrange(0, 400 - snake_bloc) /10.0) * 10.0
    pommey = round(randrange(0, 400 - snake_bloc) /10.0) * 10.0
    

    while not game_over:
        
        while jeu_ferme == True:
            screen.fill(vert)
            message("Perdu !", blanc)
            pg.display.update()
            time.sleep(2)
            pg.display.update()
            message("Q pour quitter, R pour rejouer", blanc)
            pg.display.update()
            
            
            for event in pg.event.get():
                if event.type == pg.KEYDOWN:
                    if event.key == pg.K_q:
                        game_over = True
                        jeu_ferme = False
                    if event.key == pg.K_r:
                        Jeu()
            
        for event in pg.event.get():
            if event.type == pg.QUIT:
                 game_over = True
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_LEFT:
                    x1_change = -snake_bloc
                    y1_change = 0
                elif event.key == pg.K_RIGHT:
                    x1_change =  snake_bloc
                    y1_change = 0
                elif event.key == pg.K_UP:
                    y1_change  = -snake_bloc
                    x1_change = 0
                elif event.key == pg.K_DOWN:
                    y1_change =  snake_bloc
                    x1_change = 0
        if x1 >= 400 or x1 < 0 or y1 >= 400 or y1 < 0:
            jeu_ferme = True
    
        x1 += x1_change
        y1 += y1_change
        screen.fill(vert)
        pg.draw.rect(screen, bleu, [pommex, pommey, snake_bloc, snake_bloc])
        pg.draw.rect(screen, noir, [x1, y1, snake_bloc, snake_bloc])
        pg.display.update()
        
        if x1 == pommex and y1 == pommey:
            print("Miam!!")
        
        clock.tick(snake_vitesse)
    
    pg.quit()
    quit()
    
Jeu()







